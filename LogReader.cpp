// Утилита разбора логов
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>

const std::string logLable = "~#";  // метка лога
const int numberOfTypes = 5;        // количество типов записей

// перечисляемый тип ниже содержит возможные типы записей лога 
enum
{               
    TRACE, 
    INFO, 
    DEBUG, 
    WARN, 
    ERROR
}; 

using namespace std; 

int main( int argc, char *argv[] ) 
{
    // если пользователь забыл ввести имя log файла
    if( argc != 2 )
    {
        cerr << "ошибка: нет входных файлов\n";
        return 1;
    }

    ifstream logFile(argv[1]); 
    
    vector<int> numberOfRecords(numberOfTypes, 0);   // счётчик записей каждого типа

    string timeOfFirstRecord;       // будет хранить дату и время первой записи лога
    string timeOfLastRecord;        // будет хранить дату и время последней записи лога 

    bool isFirstLogRecordReaded = false;    // содержит false если первая запись лога не считана
    string currentLine;
    while( !logFile.eof() )
    {
        getline(logFile, currentLine);
        // проверяем является ли строка записью лога
        if( currentLine.substr(0, 1) == logLable.substr(0, 1) ) 
        {   
            if( !isFirstLogRecordReaded ) 
            {
                isFirstLogRecordReaded = true;
                timeOfFirstRecord = currentLine.substr(12, 24); 
            }
            // извлекаем символы с 13-го по 36-й включительно (подстрока с датой и временем записи)
            timeOfLastRecord = currentLine.substr(12, 24);
            // если строка является записью лога,
            // то её 39-й (считая с единицы) символ определит какой приоритет у этой записи
            switch( currentLine[38] )
            {
                case 'T':
                    numberOfRecords[TRACE]++; 
                    break; 
                case 'I':
                    numberOfRecords[INFO]++; 
                    break; 
                case 'D':
                    numberOfRecords[DEBUG]++; 
                    break; 
                case 'W':
                    numberOfRecords[WARN]++; 
                    break; 
                case 'E':
                    numberOfRecords[ERROR]++; 
                    break; 
                default:
                    cerr << "ошибка: некорректная строка\n"; 
            }
        }
    }

    cout << "Период записей в логе:  " <<  endl;
    cout << "" << timeOfFirstRecord << " - " << timeOfLastRecord << endl << endl;
    cout << "Число записей каждого типа:" << endl;
    cout << "TRACE: " << numberOfRecords[TRACE] << endl; 
    cout << "INFO:  " << numberOfRecords[INFO]  << endl; 
    cout << "DEBUG: " << numberOfRecords[DEBUG] << endl; 
    cout << "WARN:  " << numberOfRecords[WARN]  << endl; 
    cout << "ERROR: " << numberOfRecords[ERROR] << endl;
    return 0; 
}